FROM python:2.7
WORKDIR /app
ADD index.html .

EXPOSE 80

CMD ["python","-m","SimpleHTTPServer","80"]
